from flask import Flask
from flask import render_template, request
import requests
import urllib.parse

app = Flask(__name__)

host_url = "https://192.168.1.216"
api_url = host_url + "/api/public"
api_key = "JgcKL1glyQrS"
api_key_secret = "XnPMU24uqgLPj4C1jm6MOXnlUBrDfqog"


@app.route('/')
def index():
    return render_template('index.html', title='Home')


@app.route('/request_kasm')
def request_kasm():
    response = ""
    parsed_json = {}
    # Optional User id can be specified
    # user_id = ""
    # optional Image_id can be specified
    # image_id = ""
    # Optional enable_sharing can be specified to turn on sharing of the session
    request_data = {
        "api_key": api_key,
        "api_key_secret": api_key_secret,
        # "user_id": "",
        # "image_id: "",
        "enable_sharing": True
    }

    res = requests.post(api_url + '/request_kasm', json=request_data, verify=False)
    try:
        parsed_json = res.json()
        print(parsed_json)
        if 'user_id' in parsed_json:
            user_id = parsed_json['user_id']
        if 'kasm_url' in parsed_json:
            parsed_json['encoded_url'] = urllib.parse.quote(parsed_json['kasm_url'])
    except:
        response = res.text
    return render_template('request.html', title='Request Kasm', message=parsed_json, error=response, host_url=host_url)


@app.route('/get_kasm_status')
def get_kasm_status():
    response = ""
    parsed_json = {}
    kasm_id = request.args.get('kasm_id')
    user_id = request.args.get('user_id')
    request_data = {
        "api_key": api_key,
        "api_key_secret": api_key_secret,
        "user_id": user_id,
        "kasm_id": kasm_id
    }

    res = requests.post(api_url + '/get_kasm_status', json=request_data, verify=False)
    try:
        parsed_json = res.json()
        print(parsed_json)
        if 'kasm_url' in parsed_json:
            parsed_json['encoded_url'] = urllib.parse.quote(parsed_json['kasm_url'])
    except:
        response = res.text
    return render_template('kasm.html', title='Get Kasm Status', message=parsed_json, error=response)


@app.route('/destroy_kasm')
def destroy_kasm():
    response = ""
    parsed_json = {}
    kasm_id = request.args.get('kasm_id')
    user_id = request.args.get('user_id')
    request_data = {
        "api_key": api_key,
        "api_key_secret": api_key_secret,
        "user_id": user_id,
        "kasm_id": kasm_id
    }
    res = requests.post(api_url + '/destroy_kasm', json=request_data, verify=False)
    try:
        parsed_json = res.json()
        print(parsed_json)
    except:
        response = res.text
    return render_template('delete.html', title='Deleted Kasm', message=parsed_json, error=response)


@app.route('/kasms')
def get_kasms():
    response = ""
    parsed_json = {}
    request_data = {
        "api_key": api_key,
        "api_key_secret": api_key_secret,
    }

    res = requests.post(api_url + '/get_kasms', json=request_data, verify=False)
    try:
        parsed_json = res.json()
        print(parsed_json)
    except:
        response = res.text
    return render_template('kasms.html', title='Request Kasm', message=parsed_json, error=response)


@app.route('/session')
def session():
    kasm_url = request.args.get('kasm_url')
    kasm_url = host_url + kasm_url
    return render_template('session.html', title='Kasm', message=kasm_url)


if __name__ == '__main__':
    app.run()

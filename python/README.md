# Example Flask App

## Prerequisites

* Kasm 1.7.0+
* Generate API key via the Kasm UI
* Enable **allow_sharing** on the All User's Group
* Set the **default_image** setting on the All Users's Group

## Flask Setup

* Create a virtual environment

        cd ~/kasm_api_examples/python/Flask
        sudo apt-get install python3-venv
        python3 -m venv venv
        . venv/bin/activate


* Install requirements

        pip install Flask
        pip install requests


* Update host url and api keys in app.py

        host_url = "https://192.168.1.216"
        api_key = "JgcKL1glyQrS"
        api_key_secret = "XnPMU24uqgLPj4C1jm6MOXnlUBrDfqog"



* Run app
   
        flask run --host 0.0.0.0


